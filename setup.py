"""Sets up the VideoSegmenter package installation"""

from setuptools import find_packages, setup

# .. Dependency lists .........................................................

INSTALL_DEPS = [
    "ruamel-yaml",
    "pydantic",
    "click",
    "tempora",
]

# Dependencies for running tests and general development of utopya
TEST_DEPS = [
    "pytest",
    "pytest-cov",
    "pre-commit",
]

# Dependencies for building the utopya documentation
DOC_DEPS = [
    "sphinx>=4.5,<5",
    "sphinx-book-theme",
    "sphinx-togglebutton",
    "ipython>=7.0",
    "myst-parser[linkify]",
    "sphinx-click",
    "pytest",
]

# .............................................................................


def find_version(*file_paths) -> str:
    """Tries to extract a version from the given path sequence"""
    import codecs
    import os
    import re

    def read(*parts):
        """Reads a file from the given path sequence, relative to this file"""
        here = os.path.abspath(os.path.dirname(__file__))
        with codecs.open(os.path.join(here, *parts), "r") as fp:
            return fp.read()

    # Read the file and match the __version__ string
    file = read(*file_paths)
    match = re.search(r"^__version__\s?=\s?['\"]([^'\"]*)['\"]", file, re.M)
    if match:
        return match.group(1)
    raise RuntimeError("Unable to find version string in " + str(file_paths))


# .............................................................................


setup(
    name="VideoSegmenter",
    #
    # Package information
    version=find_version("VideoSegmenter", "__init__.py"),
    #
    description="Splits videos into segments using ffmpeg",
    url="https://gitlab.com/blsqr/video-segmenter",
    author="Yunus Sevinchan",
    author_email="Yunus Sevinchan <blsqr0@gmail.com>",
    classifiers=[
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        #
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        #
        "Topic :: Scientific/Engineering",
    ],
    #
    # Package content
    packages=find_packages(exclude=("tests",)),
    # package_data=dict(VideoSegmenter=["cfg/*.yml"]),
    data_files=[("", ["README.md", "COPYING", "COPYING.LESSER"])],
    #
    # Dependencies
    install_requires=INSTALL_DEPS,
    extras_require=dict(
        test=TEST_DEPS,
        doc=DOC_DEPS,
        dev=TEST_DEPS + DOC_DEPS,
    ),
    #
    # Command line scripts
    entry_points={
        "console_scripts": [
            "vidseg = VideoSegmenter._cli:vidseg",
        ],
    },
)
