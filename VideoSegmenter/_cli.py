"""Implements a click-based CLI for the video segmenter package"""

import copy
import os

import click


@click.group(
    help=(
        "Video Segmenter can split videos into several segments using FFMPEG."
    ),
    epilog=(
        "Copyright (C) 2023  Yunus Sevinchan\n\n"
        "This package is free software and comes with absolutely no warranty. "
        "You are welcome to redistribute it under the conditions specified "
        "in the LGPLv3+ license.\n\n"
        "For more information and bug reports, visit:\n\n"
        "https://gitlab.com/blsqr/video-segmenter"
    ),
)
def vidseg():
    pass


@click.command(help="Segment a single video file")
@click.argument("input_video")
@click.option(
    "-s",
    "--segment-times",
    required=True,
    help="The segmentation times; these are equivalent to the starting times of the individual segments. This entry *can* start with 0, but if it does not, the zero is automatically added in the beginning.",
)
@click.option("-l", "--labels")
@click.option("-o", "--out-path")
@click.option("-y/-n", "--overwrite/--no-overwrite")
@click.option("-v", "--verbose", count=True)
def single(
    input_video: str,
    *,
    segment_times: str,
    labels: str = None,
    out_path: str = None,
    overwrite: bool = False,
    verbose: int = 0,
):
    from .vidseg import VideoSegmenter

    vidseg = VideoSegmenter.contiguous(
        input_video,
        times=segment_times.split(","),
        out_path=out_path,
        labels=labels.split(","),
    )
    vidseg.run(overwrite=overwrite, verbose=verbose)


@click.command(
    help=(
        "Segment multiple video files, reading specifications from a config "
        "file"
    )
)
@click.argument("cfg_file_path")
@click.option("-y/-n", "--overwrite/--no-overwrite")
@click.option("-v", "--verbose", count=True)
def multi(cfg_file_path: str, *, overwrite: bool = False, verbose: int = 0):
    import ruamel.yaml

    from .vidseg import VideoSegmenter

    yaml = ruamel.yaml.YAML(typ="safe")

    with open(cfg_file_path, "r") as f:
        cfg = yaml.load(f)

    # Remove some possibly existing entries where placeholders could be stored
    cfg.pop(".", None)
    cfg.pop("_", None)

    # There can be shared arguments
    shared_spec = cfg.pop("_shared", {})
    # Now, each entry in `cfg` corresponds to one video filename

    # Use the config file's directory as base directory
    abs_cfg_file_path = os.path.abspath(cfg_file_path)
    base_dir = os.path.dirname(abs_cfg_file_path)

    print(f"\n=== Segmentation tasks on {len(cfg)} file(s) ===")
    print(f"Base directory:\n  {base_dir}")
    print(f"Shared configuration:\n  {shared_spec}")
    print(f"Files:\n" + "\n".join(f"  {f}" for f in cfg.keys()))
    print("\n\n=== Now processing videos ... ===")

    for i, (video_fpath, spec) in enumerate(cfg.items()):
        kwargs = copy.deepcopy(shared_spec)
        kwargs.update(spec)
        print(f"--- Task {i+1} ---")
        print(f"Task-specific parameters:\n  {spec}")

        vidseg = VideoSegmenter.contiguous(
            os.path.join(base_dir, video_fpath),
            **spec,
        )
        vidseg.run(overwrite=overwrite, verbose=verbose)

    print(f"=== Finished segmentation of {len(cfg)} video(s) ===\n")


vidseg.add_command(single)
vidseg.add_command(multi)
