"""Implementation of the video segmentation functionalities"""

import copy
import glob
import os
import re
import shutil
import subprocess
from typing import Dict, List, Optional, Tuple, Union

import pydantic

from .tools import format_timedelta, timedelta_to_seconds

# -----------------------------------------------------------------------------

DEFAULT_OUT_PATH: str = "{name}_{label_or_no}.{ext}"
"""A format string for the output path"""

FFMPEG_DEFAULT_KWARGS: dict = {
    "c": "copy",
    "reset_timestamps": True,
    "g": True,
    "map": 0,
}
"""The default ffmpeg parameters for segmentation

These parameters are always taken into account, but a user is able to update
them for each segmentation task.
"""

# -----------------------------------------------------------------------------


class Segment(pydantic.BaseModel):
    """A descriptor for an individual segment of a video"""

    segment_no: int
    time_start: float
    time_end: Optional[float]
    label: Optional[str] = None
    out_path: Optional[str] = None
    discard: Optional[bool] = False

    @pydantic.validator("time_start", "time_end", pre=True)
    def timedelta_to_seconds(
        cls, v: Union[str, float, int]
    ) -> Union[float, int]:
        """Converts various formats for specifying time durations into a
        uniform format: a numeric type representing the time in seconds."""
        if v is None:
            return v
        return timedelta_to_seconds(v)

    @property
    def info(self) -> str:
        """An informational string that describes this segment"""

        def fmt_t(t: Optional[float]) -> str:
            if t is None:
                return "end"

            elif t == 0:
                return "start"

            return format_timedelta(t)

        s = f"Segment {self.segment_no:<2d} "
        s += f"({fmt_t(self.time_start)} → {fmt_t(self.time_end)})"
        return s

    def generate_out_path(self, **fstr_kwargs) -> str:
        """Creates the output path for this segment using the internally
        stored ``out_path`` (which can be a format string) and externally
        provided formatting information."""
        out_path = self.out_path if self.out_path else DEFAULT_OUT_PATH
        out_path = os.path.expanduser(out_path)
        return out_path.format(
            no=self.segment_no,
            label=self.label,
            label_or_no=(
                self.label if self.label is not None else self.segment_no
            ),
            time_start=self.time_start,
            time_end=self.time_end,
            **fstr_kwargs,
        )


class VideoSegmenter(pydantic.BaseModel):
    """Data class to hold information on the video that is to be segmented"""

    path: pydantic.FilePath
    segments: List[Segment]
    ffmpeg_cmd: str = "ffmpeg"
    ffmpeg_kwargs: dict = {}

    seg_prefix: str = "__seg"
    seg_fstr: str = "%03d"  # old style format string (for ffmpeg)
    tmp_name_fstr: str = ".tmp.{video_name}{seg_prefix}{seg_fstr}.{ext}"
    tmp_name_pat: str = r"^.*{seg_prefix}(\d+)\.{ext}$"

    # .. Constructors .........................................................

    @classmethod
    def contiguous(
        cls,
        path: str,
        *,
        times: Union[List[float], Dict[str, str]],
        out_path: Optional[str] = None,
        labels: List[str] = None,
    ) -> "VideoSegmenter":
        """Factory class method to create contiguous segments with
        *separation* times specified.

        Args:
            path (str): The path to the video file that is to be segmented
            times (List[float]): The segmentation times, i.e. at which times to
                *split* the input video. The number of segments will be the
                number of segmentation times plus one.
            out_path (str, optional): A path format string to use for the
                output segments. Relative files are regarded as relative to the
                input filepath.
            labels (List[str], optional): A sequence of labels to use for the
                individual segments. Needs to be of the same length as the
                number of segments.
        """
        if isinstance(times, dict):
            if labels is not None:
                raise ValueError(
                    "Cannot specify `labels` if `times` are given as dict!"
                )

            # Need to ensure proper sorting, even if the times are given as a
            # string like "3m 12s". Thus, first convert each key to be the
            # segmentation time in seconds (as a numeric type)
            times = {
                timedelta_to_seconds(t): label for t, label in times.items()
            }
            _times = sorted(list(times.keys()))
            labels = [times[t] for t in _times]
            times = _times

        else:
            times = [timedelta_to_seconds(t) for t in times]

        if times[0] != 0:
            times = [0] + times

        if labels is None:
            labels = [None for _ in times]

        elif len(labels) != len(times):
            raise ValueError(
                "Number of labels need be of the same length as the number "
                f"of segments. Have {len(times)} segments ({times}) "
                f"but {len(labels)} labels:  {labels}"
            )

        segments = []
        it = zip(times, times[1:] + [None], labels)

        for i, (time_start, time_end, label) in enumerate(it):
            seg = Segment(
                segment_no=i,
                time_start=time_start,
                time_end=time_end,
                out_path=out_path,
                label=label,
            )
            segments.append(seg)

        return cls(path=path, segments=segments)

    # .. Properties ...........................................................

    @property
    def segment_times(self) -> List[float]:
        """Returns all segment times"""
        return [seg.time_start for seg in self.segments]

    def __len__(self) -> int:
        """Number of segments of this video"""
        return len(self.segments)

    # .. Performing segmentation ..............................................

    def run(self, *, overwrite: bool = False, verbose: int = 0):
        """Performs the actual segmentation by starting an ffmpeg subprocess.

        Args:
            overwrite (bool, optional): Whether to overwrite potentially
                existing files in the target location.
            verbose (int, optional): Verbosity. At 0, there will be no output
                from the subprocess, unless there was an error.
                At 1, the output will be shown after the process finished.
                At 2, the output will be piped through.

        Raises:
            RuntimeError: On FFMPEG subprocess failing or producing an
                unexpected number of temporary output files.
        """

        # TODO implement logging instead of print statements

        def show_stdout(proc):
            if not proc.stdout and not proc.stderr:
                return
            output = proc.stdout if proc.stdout else proc.stderr
            print(f"\n--- output:\n{output}")
            print("--- end of process output\n")

        # Determine paths, extension etc
        video_dir = os.path.dirname(os.path.abspath(self.path))
        video_basename = os.path.basename(self.path)
        video_name, ext = os.path.splitext(video_basename)
        ext = ext[1:]

        # And define a temporary file name from that
        tmp_name = self.tmp_name_fstr.format(
            video_name=video_name,
            ext=ext,
            seg_prefix=self.seg_prefix,
            seg_fstr=self.seg_fstr,
        )

        # Get the ffmpeg segmentation command and run the subprocess with it
        cmd = self._assemble_ffmpeg_cmd(
            tmp_name=tmp_name, video_basename=video_basename
        )

        print(
            f"Running segmentation ...\n"
            f"  Video:       {self.path}\n"
            f"  # segments:  {len(self)}\n"
            f"  command:     {' '.join(cmd)}"
        )

        proc = subprocess.run(
            cmd, cwd=video_dir, capture_output=(verbose < 2), text=True
        )

        if proc.returncode != 0:
            show_stdout(proc)
            raise RuntimeError(
                "FFMPEG command invocation failed!\n"
                "Check that all arguments are correct and files exist.\n\n"
                f"Segmentation command:\n  {' '.join(cmd)}\n"
            )

        elif verbose == 1:
            show_stdout(proc)

        # Determine which files were generated
        tmp_files_globstr = os.path.join(
            video_dir, tmp_name.replace(self.seg_fstr, "*")
        )
        tmp_files = sorted(glob.glob(tmp_files_globstr))
        if len(tmp_files) != len(self):
            _tmp_files = "\n".join(f"  {f}" for f in tmp_files)
            show_stdout(proc)
            raise RuntimeError(
                "Unexpected number of temporary files created!\n"
                f"Expected {len(self)} but found {len(tmp_files)}:\n"
                f"{_tmp_files}\n"
                "Check the output of the ffmpeg command for debugging."
            )
        print(f"Segmented video into {len(tmp_files)} (temporary) files.")

        # ... and rename the files
        self._move_or_discard_files(
            tmp_files,
            overwrite=overwrite,
            basedir=video_dir,
            basename=video_basename,
            name=video_name,
            ext=ext,
        )

        print("Segmentation finished.\n")

    def _assemble_ffmpeg_cmd(
        self, *, tmp_name: str, video_basename: str
    ) -> Tuple[str]:
        """Assembles the subprocess command for segmentation via ffmpeg"""
        cmd = tuple()

        # Prepare keyword arguments, updating from defaults
        ffmpeg_kwargs = copy.deepcopy(FFMPEG_DEFAULT_KWARGS)
        ffmpeg_kwargs.update(self.ffmpeg_kwargs)

        # Get ffmpeg executable
        ffmpeg_cmd = shutil.which(self.ffmpeg_cmd)
        if not ffmpeg_cmd:
            raise RuntimeError(
                "Could not find a ffmpeg executable!\nMake sure it is "
                f"installed and can be invoked via '{self.ffmpeg_cmd}'."
            )
        cmd += (ffmpeg_cmd,)

        # .. input file
        cmd += ("-i", video_basename)

        # .. segmentation filter and encoding kwargs
        for k, v in ffmpeg_kwargs.items():
            if isinstance(v, bool):
                v = int(v)
            cmd += (f"-{k}", f"{v}")
        cmd += (
            "-f",
            "segment",
            "-segment_times",
            ",".join(str(s) for s in self.segment_times[1:]),
        )

        # .. output file pattern (temporary files, will later be renamed)
        cmd += (tmp_name,)

        return cmd

    def _move_or_discard_files(
        self, tmp_files: list, *, overwrite: bool, ext: str, **fstr_kwargs
    ) -> Dict[int, str]:
        """Moves or discards the temporary files that were the output of the
        ffmpeg segmentation subprocess.

        Args:
            tmp_files (list): Files to move or discard
            overwrite (bool): Whether overwriting an existing file during
                moving should be allowed
            ext (str): The file extension, needed for regex pattern
            **fstr_kwargs: Further format string arguments that are used to
                determine the final output path for each video segment

        Returns:
            Dict[int, str]: A mapping from segment number to final output path.
                This will no longer include discarded segments.

        Raises:
            FileExistsError: If ``overwrite`` is not set and there already
                exists a file at the target location.
        """
        out_paths = dict()
        seg_pat = re.compile(
            self.tmp_name_pat.format(seg_prefix=self.seg_prefix, ext=ext)
        )

        print(f"Renaming (overwrite: {overwrite}) ...")

        for tmp_file in tmp_files:
            # Get the segment number
            # Use the actual file name to be sure it is the correct number and
            # it is not dependent on the order or whether files were discarded.
            seg_match = seg_pat.fullmatch(str(tmp_file))
            seg_no = int(seg_match.groups()[0])
            seg = self.segments[seg_no]

            # Should this segment be discarded?
            if seg.discard:
                os.remove(tmp_file)
                print(f"  {seg.info}  \t(discarded)")
                continue

            # Generate an output path and check if there is a collision
            out_path = seg.generate_out_path(ext=ext, **fstr_kwargs)
            if not overwrite and os.path.exists(out_path):
                raise FileExistsError(
                    f"A file already exists at output path:\n  {out_path}\n"
                    "Either delete it, choose a different name, or set the "
                    "`overwrite` flag."
                )
            out_paths[seg_no] = out_path

            # Move the file to its destination location
            os.makedirs(
                os.path.dirname(os.path.abspath(out_path)), exist_ok=True
            )
            shutil.move(tmp_file, out_path)
            print(f"  {seg.info}  \t{out_path}")

        return out_paths
