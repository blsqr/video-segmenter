"""Various tools"""

import datetime
from typing import Union

import tempora

# -----------------------------------------------------------------------------


def timedelta_to_seconds(
    t: Union[datetime.timedelta, str, float, int]
) -> float:
    """Converts a time duration that is given in a string-based specification
    into a numeric data type, representing the number of seconds.

    Uses :py:func:`tempora.parse_timedelta` for parsing string-like arguments.

    Args:
        t (Union[datetime.timedelta, str, float, int]): The duration to
            convert into a numeric data type

    Returns:
        float: Time duration in seconds
    """
    if isinstance(t, datetime.timedelta):
        return t.total_seconds()

    try:
        return float(t)
    except:
        pass

    return tempora.parse_timedelta(t).total_seconds()


def format_timedelta(dt: Union[int, float, datetime.timedelta]) -> str:
    """Given a duration (in seconds or as timedelta), formats it into a string.

    Args:
        dt (Union[int, float, datetime.timedelta]): The duration in seconds or
            as timedelta object.

    Returns:
        str: The formatted duration
    """
    if not isinstance(dt, datetime.timedelta):
        dt = datetime.timedelta(seconds=dt)

    s = str(dt)

    # Cut off trailing zeros
    if "." in s:
        while s.endswith("0"):
            s = s[:-1]

    return s
