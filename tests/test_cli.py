"""Tests package import"""

import pytest
from click.testing import CliRunner

import VideoSegmenter as vs
import VideoSegmenter._cli as cli

runner = CliRunner()
invoke_cli = lambda *a, **kw: runner.invoke(cli.vidseg, *a, **kw)


# -----------------------------------------------------------------------------


def test_help():
    res = invoke_cli("--help")
    assert res.exit_code == 0


def test_single():
    res = invoke_cli("single", "testfile.mp4")
    assert res.exit_code != 0


def test_multi():
    res = invoke_cli("multi", "segmentations.yml")
    assert res.exit_code != 0
