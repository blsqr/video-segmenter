# Video Segmenter

Splits videos into segments using [ffmpeg].
The splitting instructions are stored in YAML files and include the option to rename the output files according to a pattern.

[Visit the project repository][repository].


## Installation
First, create and/or enter a Python virtual environment in which you would like to install this package.

The package can then be installed using [pip]:

```bash
pip install git+https://gitlab.com/blsqr/video-segmenter
```

The video segmentation is then available via the `vidseg` CLI command:

```bash
vidseg --help
```

## Use
### Segment a single video file
```bash
vidseg single input.mp4 -s 0,20,40
```

... will segment the input file into three contiguous segments starting at 0s, 20s, and 40s, respectively.

*Note:* If the output files already exist, pass the `-y` flag to allow overwriting them.

### Segment multiple video files
Sometimes it might be desired to define multiple segmentation parameters in a configuration file:

```yaml
# _segmentations.yml
---
# Shared parameters
_shared:
  out_path: "output_{label_or_no}.{ext}"

basic_input.mp4:       # path to the video (relative to config file)
  # Split at three times (in seconds)
  times: [0, 20, 40]

some_input.mp4:
  # Split into four segments and assign custom labels
  # Can also specify times via natural strings
  times:
    0:          id10
    00:00:20:   id11
    1m 40s:     id12
    120:        id13

some/directory/another_input.mp4:
  times: [0, 20, 100]

  # Can adjust shared parameters
  out_path: segments/seg{no:02d}.{ext}  # relative to *config* file!
```

This configuration file can then be passed to the CLI:

```bash
vidseg multi _segmentations.yml
```




## For developers
If you plan on developing for this project, installation should be done from a local `git clone` of the repository.
After having the project cloned, you may want to enter a virtual environment for development.

To then install the package (in editable mode), run:

```
cd video-segmenter
pip install -e .[dev]
```

which will include development-related dependencies (for tests and building of the documentation).

To automatically run [pre-commit][pre-commit] hooks, install the configured git hooks using `pre-commit install`.


### Running tests
Enter the virtual environment, then run [pytest][pytest]:

```bash
python -m pytest -v tests/ --cov=VideoSegmenter --cov-report=term-missing
```

### Building the documentation
```bash
cd doc
make doc
make linkcheck  # optional
make doctest    # optional
```

The documentation can then be found in [`doc/_build/html`](doc/_build/html/).

To automatically generate figures, set the `VIDEOSEGMENTER_USE_TEST_OUTPUT_DIR` environment variable before invoking `make doc`.

```bash
export VIDEOSEGMENTER_USE_TEST_OUTPUT_DIR=yes
```


## Copyright
```
Video Segmenter
(c) 2023, Yunus Sevinchan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```


[ffmpeg]: https://www.ffmpeg.org
[repository]: https://gitlab.com/blsqr/video-segmenter
[pip]: https://pip.pypa.io/en/stable/
[pytest]: https://pytest.org/
[pre-commit]: https://pre-commit.com
