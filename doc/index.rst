.. _welcome:

``Video Segmenter``
=========================================================

Splits videos into segments using ffmpeg

:py:mod:`VideoSegmenter` ... 🚧

.. note::

    If you find any errors in this documentation or would like to contribute to the project, we are happy about your visit to the `project page <https://gitlab.com/blsqr/video-segmenter>`_.


.. toctree::
    :hidden:

    Repository <https://gitlab.com/blsqr/video-segmenter>
    Changelog <https://gitlab.com/blsqr/video-segmenter/-/blob/main/CHANGELOG.md>

.. toctree::
    :caption: Reference
    :maxdepth: 2
    :hidden:

    API Reference <api/VideoSegmenter>
    index_pages
