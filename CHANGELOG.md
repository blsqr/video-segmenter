# Changelog

## 0.1

Initial release of `VideoSegmenter` package and `vidseg` CLI with the following feature set:

- Split a video into contiguous segments using ffmpeg.
- Rename the individual segments according to some pattern or using custom labels.
- Allow to discard individual segments.
- Accept a configuration file that defines multiple input files and segmentation specifications.
